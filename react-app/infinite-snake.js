var React = require('react');

var app = React.createFactory(require('../client-side/app'));
var initialState = JSON.parse(document.getElementById('initial-state').innerHTML);

React.render(
    app(initialState),
    document.getElementById('react-app')
);