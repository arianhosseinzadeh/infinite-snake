require('babel/register'); // JSX transpiler
var React = require('react'),
    app = React.createFactory(require('../client-side/app')),
    router = require('express').Router();

router.get('/', function(req, res) {
  var initialState = {};
  var markup = React.renderToString(
      app(initialState)
  );
  res.render('index', {
    markup: markup,
    state: JSON.stringify(initialState)
  });
});

module.exports = router;