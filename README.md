# Online Multiplayer Infinite-board Snake Game

An online multi-player snake game environment, in which all the players play in the a single infinite board (which has no boundaries). An arbitrary number of players can join the game and play against each other. Interval Tree was used to deal with the infinity of the board. 

## Client and Server communication: 

### Server ###

```
#!javascript

on('USERNAME') {
	if it's ok , send 'START'
		create a snake for the user
	otherwise , send 'ERROR_JOIN'
}
```

```
#!javascript

on('disconnect') {
	remove user from the user list
	remove the snake from the game 
}

```

```
#!javascript

on('DIRECTION') {
	update snake direction
}
```

```
#!javascript

on('NEW_CHAT_MESSAGE') {
	send the message to all of the users
}
```

```
#!javascript

periodicUpdate : {
	move the snake :
		if it collided with another snake, delete the snake and create a new one
	send user the new view.
        If a score gained , send the score 
}
```


### Client ###

```
#!javascript

on('START'):
	render the game board
```

```
#!javascript

on('ERROR_JOIN'):
	show the message

```

```
#!javascript

on('VIEW'):
	render the new view
```
 
```
#!javascript

on('SCORE'):
	update the score
```

```
#!javascript

on('CHAT_MESSAGE'):
	shows in the chat component
```

## Core Idea: Using Interval Trees
There's a camera (i.e. view) associated with each snake that watches it from top and moves with it on the board. The server queries the new coordinate of the snake and its surroundings (a rectangle with snake inside it) and sends the client the proper information about the content of the cells in this rectangle. (e.g. cells of other snakes or prizes or holes, etc.)
Although it is possible to use a simple array as the data structure to store the information of cells but the time complexity to perform add, remove, query actions on an array is **O(n)** which is high for the purpose of our game. On the other hand, the time complexity to do the same actions on an Interval Tree is **O(logn)**. This becomes helpful when the number of players increases, which is a probable case for an online game.Therefore, we used Interval Trees to be able to have more efficient actions and to be able to do rectangular queries. 

## Architecture
The architecture consists of two major sections : Server side & Client side. 

### Server
Snake : keeps the information related to each snake (queue of coordinates)
Game : For each player, there's a Game object residing on the server. This object periodically sends the updates to the player , and receives the applies the new direction and updates the snake. It also keeps the score
GameStore : keeps reference to a set of Game objects  
Listener: listens to the messages coming from players and triggers actions accordingly
LoginStore: stores usernames and colours in order to have them unique 
User: keeps information for each user
RTree: a wrapper around rbush instance (which is basically an interval tree) to add and remove nodes (i.e. coordinates)

### Client
App: the react app root element
LoginPage: where the user enters the username and if the username is ok (validated on server-side) is forwarded to the game 
Game : an element which references three components : 
Gameboard : which is the grid board of the game
Userslist : which shows the list of players in the game and their scores
Chat-Box : which shows the messages from players and let them send messages 

## Technologies:
In this project I used : 

* NodeJS
* ReactJS framework 
* Less.js
* Socket.io (which makes it a **live** application)
* rbush (an implementation of interval tree)

## Live Demo ##
Please check [Online Multiplayer Infinite-board Snake Game](http://infinite-snake.herokuapp.com/) to see the demo of the app.

## Run the code locally 
To run the code locally, first, clone the project on your local machine :
```sh
$ git clone git@bitbucket.org:arianhosseinzadeh/infinite-snake.git
```
Then, go to the directory of the project and install all the dependencies :
```sh
$ npm install
```
Build and run the code :
```sh
$ npm start
```
And finally check out the application in your browser [http://localhost:9091/](http://localhost:9091/)

- [How to install node ?](https://nodejs.org/download/)