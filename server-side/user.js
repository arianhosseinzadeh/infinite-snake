var User = function(userId, userName, color, socket) {
    var score;
    var _init = function() {
        score = 0;
        socket.join('IN_GAME');
        socket.emit('START_GAME', {'user_id': userId});
    };
    var leave = function() {
        socket.leave(userId);
        socket.leave('IN_GAME');
    };
    var increaseScore = function(s) {
        score += s;
    };
    var getScore = function() {
        return score;
    };
    var getColor = function() {
        return color;
    };
    _init();
    return ({
        userId: userId,
        color: color,
        leave: leave,
        userName: userName,
        socket: socket,
        increaseScore: increaseScore,
        getScore: getScore,
        getColor: getColor
    });
};
module.exports = User;