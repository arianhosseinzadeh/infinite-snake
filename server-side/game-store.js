var game = require('./game/game');
var RTree = require('./game/RTree');

var store = function(io) {
    var userIdToGame = {};
    var addUser = function(user) {
        var rTree = RTree.getInstance();
        userIdToGame[user.userId] = new game(user, io, rTree);
    };
    var removeUser = function(userId) {
        var game = _getGame(userId);
        if (game) {
            game.finish();
            delete userIdToGame[userId];
        }
    };
    var _getGame = function(userId) {
        return userIdToGame[userId]
    };
    var updateDirection = function(userId, newDirection) {
        var game = _getGame(userId);
        game.updateDirection(newDirection);
    };
    return ({
        addUser: addUser,
        removeUser: removeUser,
        updateDirection: updateDirection
    });
};

var GameStore = (function() {
    var instance;

    function createInstance(io) {
        return new store(io);
    }

    return {
        getInstance: function(io) {
            if (!instance) {
                instance = createInstance(io);
            }
            return instance;
        }
    };
})();
module.exports = GameStore;