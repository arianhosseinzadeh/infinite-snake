var LoginStore = require('./login-store');
var GameStore = require('./game-store');

module.exports = function(socket, io) {
    var loginStore = LoginStore.getInstance(io);
    var gameStore = GameStore.getInstance(io);
    var userId = null;
    socket.on('NEW_USER', function(data) {
        var userName = data['user_name'];
        if (!userName || userName.trim().length < 4 || userName.trim().length > 10) {
            socket.emit('ERROR_JOIN', {message: 'Invalid username'});
        } else if (loginStore.userNameExists(userName)) {
            socket.emit('ERROR_JOIN', {message: 'Duplicate username'});
        } else {
            var newUser = loginStore.addUser(data['user_name'], socket);
            userId = newUser.userId;
            gameStore.addUser(newUser);
        }
    });
    socket.on('disconnect', function() {
        console.log('User with id : ' + userId + ' disconnected from the server');
        loginStore.deleteUser(userId);
        gameStore.removeUser(userId);
        userId = null;
    });
    socket.on('NEW_DIRECTION', function(data) {
        if (loginStore.getUserById(userId)) {
            gameStore.updateDirection(userId, data['new_direction']);
        } else {
            socket.emit('RESTART_APP');
        }
    });
    socket.on('CHAT_MESSAGE', function(data) {
        var userName = loginStore.getUserNameById(userId);
        if (userName) {
            io.to('IN_GAME').emit('CHAT_MESSAGE', {
                'user_name': userName,
                'user_id': userId,
                'chat_message': data['chat_message'],
                'color': loginStore.getUserById(userId).getColor()
            })
        } else {
            socket.emit('RESTART_APP');
        }
    });
};