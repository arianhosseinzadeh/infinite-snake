var Common = require('../../common/common');
var Snake = function(x, y) {
    var direction = null;
    var queue = null;
    var _init = function(x, y) {
        direction = Common.generateRandomDirection();
        queue = [];
        var index = 0;
        switch (direction) {
            case Common.LEFT:
                for (index = 0; index < Common.initialSnakeLength; index++) {
                    insert(x - index, y);
                }
                break;
            case Common.UP:
                for (index = 0; index < Common.initialSnakeLength; index++) {
                    insert(x, y + index);
                }
                break;
            case Common.RIGHT:
                for (index = 0; index < Common.initialSnakeLength; index++) {
                    insert(x + index, y);
                }
                break;
            case Common.DOWN:
                for (index = 0; index < Common.initialSnakeLength; index++) {
                    insert(x , y - index);
                }
                break;
        }
    };
    var getHead = function() {
        return queue[0];
    };
    var insert = function(x, y) {
        queue.unshift({x: x, y: y});
    };
    var remove = function() {
        return queue.pop();
    };
    var _setDirection = function(keyState) {
        if (keyState === Common.KEY_LEFT && direction !== Common.RIGHT) {
            direction = Common.LEFT;
        }
        if (keyState === Common.KEY_UP && direction !== Common.DOWN) {
            direction = Common.UP;
        }
        if (keyState === Common.KEY_RIGHT && direction !== Common.LEFT) {
            direction = Common.RIGHT;
        }
        if (keyState === Common.KEY_DOWN && direction !== Common.UP) {
            direction = Common.DOWN;
        }
    };
    var getSnake = function() {
        return queue;
    };
    var move = function(keyState) {
        _setDirection(keyState);
        var head = getHead();
        var headX = head.x;
        var headY = head.y;
        switch (direction) {
            case Common.LEFT:
                headX--;
                break;
            case Common.UP:
                headY++;
                break;
            case Common.RIGHT:
                headX++;
                break;
            case Common.DOWN:
                headY--;
                break;
        }
        return {x: headX, y: headY};
    };
    var getBoundaries = function() {
        var minX = Number.MAX_VALUE;
        var minY = Number.MAX_VALUE;
        var maxX = Number.MIN_VALUE;
        var maxY = Number.MIN_VALUE;
        queue.forEach(function(n) {
            minX = Math.min(minX, n.x);
            minY = Math.min(minY, n.y);
            maxX = Math.max(maxX, n.x);
            maxY = Math.max(maxY, n.y);
        });
        return {
            minX: minX,
            minY: minY,
            maxX: maxX,
            maxY: maxY
        };
    };
    _init(x, y);
    return ({
        insert: insert,
        remove: remove,
        move: move,
        getSnake: getSnake,
        getBoundaries: getBoundaries,
        getHead: getHead
    });
};
module.exports = Snake;
