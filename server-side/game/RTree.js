var rbush = require('rbush');
var minX = 0, minY = 0, maxX = 100, maxY = 100;
var numberOfNodes = 0;
var tree = rbush(5);
var densityConst = 0.2;
var extension = 5;
var rtree = function() {
    var _exceedsDensity = function() {
        var area = (maxX - minX) * (maxY - minY);
        return (numberOfNodes / area) > densityConst;
    };
    var _randomGenerator = function(min, max) {
        var l = max - min;
        return Math.floor(Math.random() * l) + min;
    };
    var findRandomEmptyCell = function() { // return array of [x,y,x,y]
        var randomPoint = {};
        do {
            randomPoint.x = _randomGenerator(minX, maxX);
            randomPoint.y = _randomGenerator(minY, maxY);
        } while (tree.search(randomPoint.x, randomPoint.y, randomPoint.x, randomPoint.y).length !== 0);
        return randomPoint;
    };
    var addRandomNode = function(value, color) { // return [x,y,x,y,{'value':value}]
        if (_exceedsDensity()) {
            maxX += extension;
            maxY += extension;
            minY -= extension;
            minX -= extension;
        }
        var randomItem = findRandomEmptyCell(minX, minY, maxX, maxY);
        tree.insert([randomItem.x, randomItem.y, randomItem.x, randomItem.y, {'value': value, 'color': color}]);
        return {
            x: randomItem.x,
            y: randomItem.y
        };
    };
    var getRange = function(xMin, yMin, xMax, yMax) {
        return tree.search([xMin, yMin, xMax, yMax]);
    };
    var getNode = function(x, y) {
        return tree.search([x, y, x, y]);
    };
    var getNodeContent = function(x, y) {
        var result = getNode(x, y);
        if (result.length === 0) {
            return undefined;
        }
        return result[0][4].value;
    };
    var removeNode = function(x, y) {
        tree.remove(tree.search([x,y,x,y])[0]);
        numberOfNodes--;
    };
    var insertNode = function(x, y, value, color) {
        tree.insert([x, y, x, y, {'value': value, 'color': color}]);
        numberOfNodes++;
    };
    return ({
        getNode: getNode,
        insertNode: insertNode,
        addRandomNode: addRandomNode,
        getRange: getRange,
        removeNode: removeNode,
        getNodeContent: getNodeContent,
        tree: tree, // get rid of this [todo]
        findRandomEmptyCell: findRandomEmptyCell
    });
};

var RTree = (function() {
    var instance;

    function createInstance() {
        return new rtree();
    }

    return {
        getInstance: function() {
            if (!instance) {
                instance = createInstance();
            }
            return instance;
        }
    };
})();
module.exports = RTree;