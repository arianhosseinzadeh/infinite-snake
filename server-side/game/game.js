var Common = require('../../common/common');
var Snake = require('./snake');
var padding = 3;
var game = function(user, io, RTree) {
    var snake, keyState, timerId, socket;
    var xLeft, xRight, yTop, yBottom;
    var _init = function() {
        socket = user.socket;
        for (var pIndex = 0; pIndex < 10; pIndex++) {
            _setPrize();
        }
        /*        var node = RTree.addRandomNode(user.userId, user.color);
         snake = new Snake(node.x, node.y);*/
        _initSnake();
        _initBoundaries();
        socket.emit('START_GAME', {'user_id': user.userId, 'color': user.getColor()});
        _periodicBroadcast();
    };
    var _initSnake = function() {
        var randomHead = RTree.findRandomEmptyCell();
        snake = new Snake(randomHead.x, randomHead.y);
        snake.getSnake().forEach(function(node) {
            RTree.insertNode(node.x, node.y, user.userId, user.color);
        });
    };
    var _initBoundaries = function() {
        var snakeHead = snake.getHead();
        xLeft = snakeHead.x - (Common.columns / 2 - 1);
        xRight = snakeHead.x + (Common.columns / 2);
        yTop = snakeHead.y + (Common.rows / 2 - 1);
        yBottom = snakeHead.y - (Common.rows / 2);
/*        console.log(xLeft + ':' + xRight);
        console.log(snakeHead.x);
        console.log(yBottom + ' : ' + yTop);
        console.log(snakeHead.y);*/
    };
    var _updateBoundaries = function(newX, newY) {
        if (yTop - newY < Common.viewWindowPadding) {
            yTop++;
            yBottom++;
        }
        if (xRight - newX < Common.viewWindowPadding) {
            xRight++;
            xLeft++;
        }
        if (newY - yBottom < Common.viewWindowPadding) {
            yBottom--;
            yTop--;
        }
        if (newX - xLeft < Common.viewWindowPadding) {
            xLeft--;
            xRight--;
        }
    };
    var finish = function() {
        //clean up the grid
        _removeSnake();
        //end of listening to actions
        socket.removeAllListeners('NEW_DIRECTION');
        //stop the timer !!!!!
        clearInterval(timerId);
    };
    var _removeSnake = function() {
        var snakeQueue = snake.getSnake();
        snakeQueue.forEach(function(node) {
            RTree.removeNode(node.x, node.y);
        });
    };
    var updateDirection = function(ks) {
        keyState = ks;
    };
    var _move = function() {
        var newPosition = snake.move(keyState); // fix this
        var cellContent = RTree.getNodeContent(newPosition.x, newPosition.y);
        if (!!cellContent && cellContent !== Common.PRIZE) {
            //restart the snake and start it from somewhere else
            _removeSnake();
            /*var node = RTree.addRandomNode(user.userId, user.color);
             snake = new Snake(node.x, node.y);*/
            _initSnake();
            _initBoundaries();
        } else {
            if (cellContent === Common.PRIZE) {
                // it's a prize no need to remove the tail
                RTree.removeNode(newPosition.x, newPosition.y);
                _setPrize();
                user.increaseScore(1);
                io.to('IN_GAME').emit('SCORE', {'user_id': user.userId, 'score': user.getScore()})
            } else {
                var p = snake.remove(); // remove tail
                RTree.removeNode(p.x, p.y);
            }
            snake.insert(newPosition.x, newPosition.y);
            RTree.insertNode(newPosition.x, newPosition.y, user.userId, user.color);
            _updateBoundaries(newPosition.x, newPosition.y);
        }
        //console.log(RTree.tree.all());
/*        return {
            'status': 'CONTINUE'
        };*/
    };
    var _setPrize = function() {
        RTree.addRandomNode(Common.PRIZE, Common.PRIZE_COLOR);
    };
    var _getView = function() {
        var arrayGrid = RTree.getRange(xLeft, yBottom, xRight, yTop);
        var view = {};
        arrayGrid.forEach(function(node) {
            var coordinate = Common.convert2dTo1d(yTop - node[1], node[0] - xLeft, xRight - xLeft + 1);
            //this is the relative coordinate to the top-left point
            view[coordinate] = {
                value: node[4].value,
                color: node[4].color
            };
        });
        return view;
    };
    var _periodicBroadcast = function() {
        timerId = setInterval(function() {
            _move();
            var boundaries = snake.getBoundaries();
            var view = _getView(boundaries.minX, boundaries.minY, boundaries.maxX, boundaries.maxY);
            socket.emit('NEW_VIEW', {'grid': view});
        }, 1000 / Common.gameSpeed);
    };
    _init();
    return ({
        finish: finish,
        updateDirection: updateDirection
    });
};

module.exports = game;