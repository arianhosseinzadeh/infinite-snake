var User = require('./user');
var randomColor = require('randomcolor');
var Common = require('../common/common');
var shortid = require('shortid');
var store = function(io) {
    var Users = {};
    var UserNames = {};
    var Colors = {};
    Colors[Common.PRIZE_COLOR] = -1; // this color is already taken
    var UserIdToColor = {};
    var addUser = function(userName, socket) {
        userName = userName.trim().toUpperCase();
        var color = _generateUniqueRandomColor();
        var userId = shortid.generate();
        var newUser = new User(userId, userName, color, socket);
        Users[userId] = newUser;
        UserNames[userName] = userId;
        io.to('IN_GAME').emit('USERS_LIST', {'users_list': getUsersList()});
        return newUser;
    };
    var _generateUniqueRandomColor = function(userId) {
        var rndColor;
        do {
            rndColor = randomColor({
                luminosity: 'bright',
                format: 'rgb' // e.g. 'rgb(225,200,20)'
            });
        } while (!!Colors[rndColor]);
        Colors[rndColor] = userId;
        UserIdToColor[userId] = rndColor;
        return rndColor;
    };
    var _removeColor = function(userId) {
        var color = UserIdToColor[userId];
        delete Colors[color];
        delete UserIdToColor[userId];
    };
    var deleteUser = function(userId) {
        if (!!getUserById(userId)) {
            Users[userId].leave();
            delete UserNames[getUserNameById(userId)];
            delete Users[userId];
            _removeColor(userId);
            io.to('IN_GAME').emit('USERS_LIST', {'users_list': getUsersList()});
        }
    };
    var userNameExists = function(userName) {
        return !!UserNames[userName.trim().toUpperCase()];
    };
    var getUserById = function(id) {
        return Users[id];
    };
    var getUsersList = function() {
        var result = [];
        for (var userId in Users) {
            if (Users.hasOwnProperty(userId)) {
                result.push({
                    'user_id': userId,
                    'user_name': Users[userId].userName,
                    'score': Users[userId].getScore(),
                    'color': Users[userId].getColor()
                });
            }
        }
        return result;
    };
    var getUserNameById = function(userId) {
        return Users[userId].userName;
    };
    return ({
        getUserById: getUserById,
        getUsersList: getUsersList,
        getUserNameById: getUserNameById,
        userNameExists: userNameExists,
        addUser: addUser,
        deleteUser: deleteUser
    });
};

var Store = (function() {
    var instance;

    function createInstance(io) {
        return new store(io);
    }

    return {
        getInstance: function(io) {
            if (!instance) {
                instance = createInstance(io);
            }
            return instance;
        }
    };
})();

module.exports = Store;