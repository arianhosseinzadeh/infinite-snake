module.exports = {
    randomGenerator: function(length) { // generates a random string with at least length of `length`
        if (length > 0) {
            var l = 0;
            var result = '';
            while (l <= length) {
                var rand = Math.random().toString(36).substr(2);
                result = result + '' + rand;
                l = l + rand.length;
            }
            return result;
        }
    },
    convert2dTo1d: function(y, x, columns) {
        return (y * columns) + x;
    },
    convert1dTo2d: function(value, columns) {
        var newCoordinate = {};
        newCoordinate.y = Math.floor(value / columns);
        newCoordinate.x = value - (newCoordinate.y * columns);
        return newCoordinate;
    },
    arrayContains: function(arr, element) {
        if (arr === undefined) {
            return false;
        }
        return arr.indexOf(element) >= 0;
    },
    deleteArrayElement: function(arr, element) {
        if (arr && arr.indexOf(element) >= 0) {
            arr.splice(arr.indexOf(element), 1);
            return arr;
        }
        return arr;
    },
    generateRandomDirection: function(){
        return Math.floor(Math.random() * 4);
    },
    EMPTY: 0, PLAYER1: 1, PLAYER2: 2, PRIZE: 3,
    LEFT: 0, UP: 1, RIGHT: 2, DOWN: 3,
    KEY_LEFT: 37, KEY_UP: 38, KEY_RIGHT: 39, KEY_DOWN: 40,
    rows: 48, columns: 60,
    gameSpeed: 8,
    boardHeight: 480, boardWidth: 600,
    PRIZE_COLOR: 'red',
    EMPTY_COLOR: 'white',
    initialSnakeLength: 4,
    viewWindowPadding: 3
};