'use strict';

var React = require('react');
var $ = require('jquery');
var msgIndex = 0;
var ChatBox = React.createClass({

    displayName: 'ChatBox',

    getInitialState: function() {
        return {
            'chat_messages': []
        };
    },

    componentDidMount: function() {
        this.props.socket.on('CHAT_MESSAGE', function(data) { //move this to chat box
            var chatMessages = this.state['chat_messages'];
            var msg = data['chat_message'];
            var userName = data['user_name'];
            msgIndex++;
            var style = {
                'color': data.color
            };
            chatMessages.push(<div key={'chat_message' + msgIndex}>
                <span style={style}>{userName}</span>
                <span className="colon"> :</span>
                <br/>
                <span style={style}>{msg}</span>
            </div>);
            this.setState({
                'chat_messages': chatMessages
            });
            $('#message_list').scrollTop(Number.MAX_VALUE);
        }.bind(this));
    },
    _sendChatMessage: function() {
        var chat_message = $('#messageBox').val();
        this.props.socket.emit('CHAT_MESSAGE', {'chat_message': chat_message});
        $('#messageBox').val('');
    },
    render: function() {
        return (<div>
            <div className="row" id="message_list">
                {this.state['chat_messages']}
            </div>
            <div className="row">
                <form role="form">
                    <div className="form-group">
                        <input type='text' id= "messageBox"/>
                    </div>
                    <div className="button-center-wrapper">
                        <input type="button" onClick={this._sendChatMessage}
                            className = "btn btn-success text-center button-center" value = "Send" />
                    </div>
                </form>
            </div>
        </div>)
    }
});

module.exports = ChatBox;
