'use strict';

var React = require('react');

var WaitPage = React.createClass({

    displayName: 'WaitPage',

    render: function() {
        return (<div className="page-center">
            <div>
                <h2 className="text-center">Connecting to the server</h2>
                <div className="progress">
                    <div className="progress-bar progress-bar-striped active" style={{width: '100%'}}></div>
                </div>
            </div>
        </div>);
    }
});

module.exports = WaitPage;
