'use strict';

var React = require('react');
var socket;
var $ = require('jquery');
var Common = require('../common/common');

var Board = React.createClass({

    displayName: 'Board',

    getInitialState: function() {
        var grid = [];
        var columns = Common.columns;
        var rows = Common.rows;
        var cellWidth = Common.boardWidth / columns;
        var cellHeight = Common.boardHeight / rows;
        for (var y = 0; y < rows; y++) {
            for (var x = 0; x < columns; x++) {
                var coordinate = Common.convert2dTo1d(y, x, columns);
                grid.push(<rect width={cellWidth} height={cellHeight}
                    x={x * cellWidth} y={y * cellHeight} fill={Common.EMPTY_COLOR}
                    key={'cell' + coordinate}
                    stroke='#E5E5E5' strokeWidth='0.5'
                />);
            }
        }
        return ({
            grid: grid,
            occupiedCells: {},
            score: 0
        });
    },
    componentDidMount: function() {
        socket = this.props.socket;
        socket.on('NEW_VIEW', function(data) {
            var newOccupiedCells = data.grid;
            var grid = this.state.grid;
            var currentOccupiedCells = this.state.occupiedCells;
            var columns = Common.columns;
            var rows = Common.rows;
            var cellWidth = Common.boardWidth / columns;
            var cellHeight = Common.boardHeight / rows;
            var coordinate;
            for (var newOccupiedCellCoordinate in newOccupiedCells) {
                if (newOccupiedCells.hasOwnProperty(newOccupiedCellCoordinate)) {
                    if (currentOccupiedCells[newOccupiedCellCoordinate] !== newOccupiedCells[newOccupiedCellCoordinate]) {
                        coordinate = Common.convert1dTo2d(newOccupiedCellCoordinate, columns);
                        var color = newOccupiedCells[newOccupiedCellCoordinate].color;
                        grid[newOccupiedCellCoordinate] = <rect width={cellWidth} height={cellHeight}
                            x={coordinate.x * cellWidth} y={coordinate.y * cellHeight} fill={color}
                            key={'cell' + newOccupiedCellCoordinate}
                            stroke='#E5E5E5' strokeWidth='0.5'/>;
                    }
                }
            }
            for (var currentOccupiedCellCoordinate in currentOccupiedCells) {
                if (currentOccupiedCells.hasOwnProperty(currentOccupiedCellCoordinate)) {
                    if (!newOccupiedCells[currentOccupiedCellCoordinate]) {
                        coordinate = Common.convert1dTo2d(currentOccupiedCellCoordinate, columns);
                        grid[currentOccupiedCellCoordinate] = <rect width={cellWidth} height={cellHeight}
                            x={coordinate.x * cellWidth} y={coordinate.y * cellHeight} fill={Common.EMPTY_COLOR}
                            key={'cell' + currentOccupiedCellCoordinate}
                            stroke='#E5E5E5' strokeWidth='0.5'/>;
                    }
                }
            }
            this.setState({
                grid: grid,
                occupiedCells: newOccupiedCells
            });
        }.bind(this));
        socket.on('SCORE', function(data) {
            var score = data['score'];
            var userId = data['user_id'];
            if (userId === this.props.userId) {
                this.setState({
                    score: score
                });
            }
        }.bind(this));
        $(document.body).on('keydown', function(event) {
            socket.emit('NEW_DIRECTION', {'new_direction': event.keyCode});
        });
    },

    render: function() {
        var scoreStyle = {
            'color': this.props.color
        };
        return (
            <div className="game-board">
                <svg className="board-svg" width={Common.boardWidth} height={Common.boardHeight}>
                    {this.state.grid}
                </svg>
                <span style={scoreStyle} className="my-score">{'Score : ' + this.state.score}</span>
            </div>
        );
    }
});

module.exports = Board;
