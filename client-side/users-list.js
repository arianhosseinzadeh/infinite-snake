'use strict';

var React = require('react');
var usersIndex = {};
var UsersList = React.createClass({

    displayName: 'UsersList',

    getInitialState: function() {
        return {
            'users_list': []
        };
    },
    _swapUserListElement: function(arr, index1, index2) {
        var tmp = arr[index1];
        arr[index] = arr[index + 1];
        arr[index + 1] = tmp;
    },
    componentDidMount: function() {
        this.props.socket.on('USERS_LIST', function(data) {
            var usersList = data['users_list'];
            usersList.sort(function(d1, d2) {
                return (d2.score - d1.score);
            });
            usersList.forEach(function(d, index) {
                usersIndex[d['user_id']] = index;
            });
            this.setState({
                'users_list': usersList
            });
        }.bind(this));
        this.props.socket.on('SCORE', function(data) {
            var score = data['score'];
            var userId = data['user_id'];
            var index = usersIndex[userId];
            if (index !== undefined) {
                var usersList = this.state['users_list'];
                if (score > usersList[index].score) {
                    usersList[index].score = score;
                    while (index > 0 && usersList[index - 1].score < score) {
                        this._swapUserListElement(usersList, index, index + 1);
                        usersIndex[usersList[index]['user_id']] = index;
                        usersIndex[usersList[index - 1]['user_id']] = index;
                        index--;
                    }
                } else {
                    usersList[index].score = score;
                    while (index < usersList.length - 1 && usersList[index + 1].score > score) {
                        this._swapUserListElement(usersList, index, index + 1);
                        usersIndex[usersList[index]['user_id']] = index;
                        usersIndex[usersList[index + 1]['user_id']] = index;
                        index++;
                    }
                }
                this.setState({
                    'users_list': usersList
                });
            }
        }.bind(this));
    },
    render: function() {
        var result = [];
        this.state['users_list'].forEach(function(d) {
            var style = {
                color: d['color']
            };
            result.push(<tr style={style} key={d['user_id']}>
                <td className="username col-md-6">{d['user_name']}</td>
                <td className="score col-md-6">{d['score']}</td>
            </tr>);
        });
        return (<table className="table border-less">
            <tbody>
                {result}
            </tbody>
        </table>);
    }
});

module.exports = UsersList;
