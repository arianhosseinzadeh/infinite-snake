'use strict';

var React = require('react');
var ioClient = require('socket.io-client');
var socket;
var LOGIN_PAGE = 0, GAME_PAGE = 1;
var WaitPage = require('./wait-page');
var LoginPage = require('./login-page');
var Game = require('./game');

var App = React.createClass({

    displayName: 'App',

    getInitialState: function() {
        return ({
            environment: LOGIN_PAGE
        });
    },
    componentWillUnmount: function() {
        socket.close();
    },
    componentDidMount: function() {
        socket = ioClient.connect();
        this.setState({
            connected: true
        });
        socket.on('START_GAME', function(data) {
            this.setState({
                environment: GAME_PAGE,
                userId: data['user_id'],
                color: data['color']
            });
        }.bind(this));
        socket.on('disconnect', function() {
            this.setState({
                environment: LOGIN_PAGE
            });
        }.bind(this));
        socket.on('RESTART_APP', function(data) {
            this.setState({
                environment: LOGIN_PAGE
            });
        }.bind(this));
    },

    render: function() {
        var result = null;
        if (this.state.connected) {
            if (this.state.environment === GAME_PAGE) { //game : true
                result = <Game socket={socket} userId={this.state.userId} color={this.state.color}/>
            } else if (this.state.environment === LOGIN_PAGE) {
                result = <LoginPage socket={socket}/>;
            }
        } else {
            result = <WaitPage />;
        }
// className : full-height is to place the div in the center
        return (<div className="full-height container app">{result}</div>)
    }
});

module.exports = App;
