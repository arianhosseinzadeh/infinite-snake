'use strict';

var React = require('react');
var ChatBox = require('./chat-box');
var UsersList = require('./users-list');
var Board = require('./board');

var Game = React.createClass({

    displayName: 'Game',

    render: function() {
        return (
            <div className="row game-page">
                <div className="col-md-2 chat-box">
                    <ChatBox socket={this.props.socket}/>
                </div>
                <div className="col-md-8">
                    <Board socket={this.props.socket} userId={this.props.userId} color={this.props.color}/>
                </div>
                <div className="col-md-2 users-list">
                    <UsersList socket={this.props.socket}/>
                </div>
            </div>
        );
    }
});

module.exports = Game;
