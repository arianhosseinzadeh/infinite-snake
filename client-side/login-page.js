'use strict';

var React = require('react');

var LoginPage = React.createClass({

    displayName: 'LoginPage',
    getInitialState: function(){
        return({
            'error_text_visible': false
        });
    },
    _sendUserName: function() {
        var userName = this.refs.userNameInputBox.getDOMNode().value;
        this.props.socket.emit('NEW_USER', {'user_name': userName});
    },

    componentDidMount: function() {
        this.props.socket.on('ERROR_JOIN', function(data) {
            var message = data.message;
            //alert(message);
            this.setState({
                'error_message': message,
                'error_text_visible': true
            });
        }.bind(this));
    },

    render: function() {
        var errorMessageVisibility = {
            visibility: this.state['error_text_visible']
        };
        return(<div className="page-center login-page">
            <div className="row">
                <div className="row">
                    <img src="images/Snake-icon.png" />
                </div>
                <div className="row">
                    <form role="form">
                        <div className="form-group">
                            <input type='text' ref="userNameInputBox" placeholder="Enter player name"
                                className = "userNameInputBox"/>
                        </div>
                        <div className="button-center-wrapper">
                            <input type="button" onClick={this._sendUserName}
                                className = "btn btn-success text-center button-center" value = "ENTER" />
                        </div>
                        <span style={errorMessageVisibility} className="error-join-message">{this.state['error_message']}</span>
                    </form>
                </div>
            </div>
        </div>);
    }
});

module.exports = LoginPage;
